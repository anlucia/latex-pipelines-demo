# Continous integration for LaTeX files

This is an example on how to configure Bitbucket Pipelines to
automagically compile some LaTeX file in your repository, and then
store the resulting PDF fine in the Download folder, so you don't have
to worry about loosing the pendrive with your presentation at some
conference.

## Set-up

1. First of all, you need to activate Bitbucket Pipelines for your
repository. On the Bitbucket Repository page, open the Setting tab and
then the Pipelines->Settings menu. You can enable the piplelines from there

2. Copy the file ```bitbucket-pipelines.yml``` in your repository and modify it accordingly.

3. In order to upload the final PDF file in the Download folder, you
   need to set up an [app password](https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html).
   This has to be done only once, since it can be shared by multiple repositories.
   After generating it, save the passord in a variable named ```UPLOAD_FILE_PASSWORD``` in the Bitbucket settings->Pipelines->Account variables tab.
   
## Configuring the ```bitbucket-pipelines.yml``` file

The pipeline works in two steps. In the first one, the PDF file is compiled. In the second one, it gets uploaded in the Download folder.

You can modify either the command that it is used to generate the PDF
file, as well as the Docker image which is used to run the
command. There are quite a few different LaTeX docker images out
there: the smallest ones are faster but lack some of the
libraries/tools you might want to use. 

One good choice is to use [blang/latex](https://github.com/blang/latex-docker)
It has a few different images you can use

  + blang/latex:ctanfull CTAN TexLive Scheme-full: Up-to-date, all packages (5.6GB)
  + blang/latex:ctanbasic CTAN TexLive Scheme-basic: Up-to-date, only basic packages, base for custom builds (500MB)
  + blang/latex:ubuntu Ubuntu TexLive distribution: Old but stable, most needed package: texlive-full (3.9GB)

You can of course generate your own image if you want and know how to do it!

## Timeout

I have set up a timeout at 6 minutes, in order to avoid the situation
where one command gets stuck (which I also try to avoid with the
command flags to latexmk, but you never know...) and it consumes a lot
of your precious Pipelines minutes.

Most of the time spend by the pipeline is preparing the latex docker
image. I wonder if this can be optimized somehow. I am not an expert
so if you know how to make this step quicker please let me know!
